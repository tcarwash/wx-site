#!flask/bin/python
from flask import Flask, flash, jsonify, render_template, Markup, request, abort
import json
from wxstation.trend import getTrend
import templates
from flask_bootstrap import Bootstrap
from time import time, sleep
import pygal
from os import environ
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField, PasswordField
import logging
import requests

app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = environ['SECRET_KEY']
Bootstrap(app)

url = "http://206.58.200.238:5000"
dataLock = "./wxstation/data/data.lock"
aprsLock = "./wxstation/data/aprs.pid"
logfile = "./wxstation/data/logs/wx.log"

"""Logging"""
logger = logging.getLogger("WEB")
logger.setLevel(logging.DEBUG)

fileHandler = logging.FileHandler(logfile)
fileHandler.setLevel(logging.DEBUG)

streamHandler = logging.StreamHandler()
streamHandler.setLevel(logging.WARNING)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fileHandler.setFormatter(formatter)
streamHandler.setFormatter(formatter)

logger.addHandler(fileHandler)
logger.addHandler(streamHandler)

class ObservationForm(Form):
    key = PasswordField('Key:', validators=[validators.required()])
    obs = TextField('Observation:', validators=[validators.required(), validators.Length(min=1, max=20)])

def aprsActive():
    try:
        response = requests.get(url + "/aprs").json()
    except:
        return False
    if response["success"] == True:
        return True
    else:
        return False

@app.route("/form/", methods=['GET', 'POST'])
def observation():
    form = ObservationForm(request.form)
    if request.method == 'POST':
        key=request.form['key']
        obs=request.form['obs']
        if form.validate():
            if requests.post(url + "/observation/?key=" + key + "&obs=" + obs).status_code == 200:
                flash("\"" + obs + "\"" + " - Was added as an observation")
            else:
                flash('Something went wrong')
        else:
            flash('Error: All the form fields are required. And max observation length is 20 characters')
    return render_template('obsForm.html', form=form)

@app.route('/', methods=['GET'])
def index():
    data = requests.get(url + "/wx/data").json()
    timestamp = []
    pres = []
    hum = []
    for item in data:
            pres.append(item['barometer'])
            timestamp.append(item['timestamp'])
            hum.append(item['humidity'])
    line_chart = pygal.Line(show_only_major_dots=True)
    line_chart.x_labels = timestamp[-1000:]
    line_chart.add('Humidity(%)', hum[-1000:])
    line_chart.add('Pressure(mbar)', pres[-1000:], secondary=True)
    graph_data = line_chart.render_data_uri()
    try:
        trend=getTrend(data)
    except:
        trend=Markup('<span class="label label-danger">Can\'t determine trend</span>')
    print(trend)
    return render_template("index.html", title="Home", data=data, trend=trend, timenow=time(), graph_data=graph_data, aprs=aprsActive())

@app.route('/wx/data', methods=['GET'])
def get_data():
    data = requests.get(url +"/wx/data").json()
    return jsonify(data)

@app.route('/usage', methods=['GET'])
def api_usage():
    return render_template("usage.html", title="API Usage")

@app.route('/about', methods=['GET'])
def about():
    return render_template("about.html", title="About the Station")

@app.route('/wx/data/latest', methods=['GET'])
def get_latest():
    data = requests.get(url + "/wx/data").json()
    data[-1]["bar-trend"] = getTrend(data)
    return jsonify(data[-1])

if __name__ == '__main__':

    app.run(host="0.0.0.0", debug=False)
